
/* 
 * 2048c: Simple implementation of the 2048 game in C
 * By InternetFox0
 * 
 * File 2048.c: Game logic
 * 
 * This code is made available under the terms of the MIT License.
 * See here for more info: https://gitlab.com/InternetFox0/2048c/blob/master/LICENSE
 */

// Project includes:
#include "2048.h"

// Standard library includes:
#include <stdio.h>   // printf
#include <stdlib.h>  // calloc, srand, rand, malloc
#include <time.h>    // time
#include <string.h>  // memcpy

// "Private member" (static) variable declarations:
static Tile_t *grid;  // The game grid

/* Note: The size of the grid (GRIDSZ) depends on SIDESZ, the number of tiles along each edge.
 *   Tile numbering begins in the upper left corner at 0, and goes left to right, top to bottom.
 *   So the traditional 4x4 grid (SIDESZ = 4) would be indexed like so:
 *      0,  1,  2,  3,
 *      4,  5,  6,  7,
 *      8,  9, 10, 11,
 *     12, 13, 14, 15
 * 
 * We can generalize the layout of the grid like so:
 *                       0, ...,     SIDESZ - 1,
 *                  SIDESZ, ..., 2 * SIDESZ - 1,
 *              2 * SIDESZ, ...,
 *                     ...,
 *   (SIDESZ - 1) * SIDESZ, ...,     GRIDSZ - 1
 * 
 * Notice that each next row is indexed SIDESZ-higher than the last, and each next column 1-higher than the last.
 * There is also a SIDESZ-related pattern in detecting edges of the grid.
 */

// Setup the tiles and their links:
void grid_init (void) {
	// Allocate enough memory for a SIDESZ-square game grid:
	grid = calloc (GRIDSZ, sizeof(Tile_t));

	// Initialize each tile in the grid with a starting value and links to adjacent tiles:
	// i is the index of the current tile in the grid
	for (int i = 0; i < GRIDSZ; i++) {
		// Every tile begins empty (value == 0):
		grid[i].value = 0;
		
		// If the tile number is lower than the size of one side, then this is the top row.
		// Otherwise, subtract the number of tiles on one side to find the index of the tile above.
		grid[i].next[UP]    = (i < SIDESZ) ? NULL : &grid[i - SIDESZ];
		
		// If the tile number is higher than the size of one side, then this is the bottom row.
		// Otherwise, add the number of tiles on one side to find the index of the tile below.
		grid[i].next[DOWN]  = (i >= (SIDESZ - 1) * SIDESZ) ? NULL : &grid[i + SIDESZ];
		
		// If the tile number is divisible by the size of one side, then this is the left column.
		// Otherwise, subtract 1 to the current tile number to find the index of the tile to the left.
		grid[i].next[LEFT]  = (i % SIDESZ == 0) ? NULL : &grid[i - 1];
		
		// If the next tile number is divisible by the size of one side, then this is the right column.
		// Otherwise, add 1 to the current tile number to find the index of the tile to the right.
		grid[i].next[RIGHT] = ((i + 1) % SIDESZ ==  0) ? NULL : &grid[i + 1];
	}

	// Seed the random number generator with the system time:
	srand (time (0));
}

// Check if the grid is full:
// Return value 0 = not full, 1 = full
int grid_ckfull (void) {
	int full = 1;
	for (int i = 0; i < GRIDSZ; i++)
		if (grid[i].value == 0)
			full = 0;
		
	return full;
}

// Randomly place either a 2 or 4 into the grid:
// Return value 0 = not full, 1 = full
int grid_rand (void) {
	// Check if the grid is already full:
	int full = grid_ckfull ();

	// If not, choose a random location for the new tile:
	if (!full) {
		int location;
		// Repeatedly choose a random tile until we find an empty one:
		do {
			location = rand () % GRIDSZ;
		} while (grid[location].value != 0);

		// Now randomly place either a 2 or a 4 there:
		grid[location].value = ((rand () % 2) + 1) * 2;
	}

	return full;
}

// Print the current state of the grid:
void grid_print (void) {
	for (int i = 0; i < GRIDSZ; i++) {
		printf ("%7u ", grid[i].value);
		if ((i + 1) % SIDESZ == 0) printf ("\n");
	}
}

// Push all tiles toward an edge in a selected direction:
// Return value -2 = invalid direction, -1 = nothing moved, 0+ = score of move
int grid_push (Dir_t dir) {
	// Score earned from this operation:
	unsigned score = 0;
	
	// Remains 0 until we succeed in moving something:
	int moved = 0;

	// Push parameters:
	Dir_t opp;   // Direction opposite of the push
	int   min;   // Index of first tile to receive the push
	int   incr;  // Increment required to advance to the next row/column
	int   max;   // Index of the final tile onto which to push

	// Set the parameters according to the selected direction:
	switch (dir) {
		case UP: {
			opp  = DOWN;        // Opposite of up is down
			min  = 0;           // Start in the upper left corner
			incr = 1;           // Advance by columns
			max  = SIDESZ - 1;  // Stop in the upper right corner
		}; break;

		case DOWN: {
			opp  = UP;                     // Opposite of down is up
			min  = (SIDESZ - 1) * SIDESZ;  // Start in the lower left corner
			incr = 1;                      // Advance by columns
			max  = GRIDSZ - 1;             // Stop in the lower right corner
		}; break;

		case LEFT: {
			opp  = RIGHT;                  // Opposite of left is right
			min  = 0;                      // Start in the upper left corner
			incr = SIDESZ;                 // Advance by rows
			max  = (SIDESZ - 1) * SIDESZ;  // Stop in the lower left corner
		}; break;

		case RIGHT: {
			opp  = LEFT;        // Opposite of right is left
			min  = SIDESZ - 1;  // Start in the upper right corner
			incr = SIDESZ;      // Advance by rows
			max  = GRIDSZ - 1;  // Stop in the lower right corner
		}; break;
		
		default: return -2;  // Signal that an invalid move was selected
	}

	// Iterate over the rows/columns:
	// i is the index of the first destination tile for this row/column
	for (int i = min; i <= max; i += incr) {
		// Source and destination pointers for making comparisons and moves:
		Tile_t *dst = &grid[i];        // Destination begins along receiving edge
		Tile_t *src = dst->next[opp];  // Source begins in the next tile away

		// Attempt to move or combine tiles until the source is out of bounds:
		while (src != NULL) {
			// If the source is empty, move on to the next:
			if (src->value == 0) {
				src = src->next[opp];
			}

			// If the destination is empty, move the source value there:
			else if (dst->value == 0) {
				dst->value = src->value;
				src->value = 0;
				src = src->next[opp];     // This source now empty - move on
				moved = 1;                // This move succeeded!
			}

			// If the source and destination are identical, combine them:
			else if (dst->value == src->value) {
				dst->value += src->value;
				score += dst->value;
				src->value = 0;
				src = src->next[opp];      // This source now empty - move on
				dst = dst->next[opp];      // Can't combine into one tile twice
				moved = 1;                 // This move succeeded!
			}

			// If both are full but unequal, try the next possible destination:
			else {
				dst = dst->next[opp];
			}

			// Don't allow the source and destination to overlap:
			if (src == dst) src = src->next[opp];
		}
	}
	
	if (!moved) return -1;  // Signal if no move was actually made
	else return score;      // Otherwise return the score of the move
}

// Check if thge game is over (no moves possible):
// Return value 0 = not over, 1 = over
int grid_ckover (void) {
	// The game cannot be over if the grid is not full:
	if (grid_ckfull () == 0) return 0;
	
	// Otherwise, we actually need to check to see if a valid move remains
	
	// Back up the grid so we can restore it after our tests:
	Tile_t *gridbak = malloc (sizeof(grid));
	memcpy (gridbak, grid, sizeof(grid));     // Backup the grid
	
	// Test each possible move:
	int over = 1;
	for (int dir = 0; dir < 4; dir++) {
		if (grid_push (dir) >= 0) {
			over = 0;  // This move didn't fail, so the game isn't over
			break;
		}
		memcpy (grid, gridbak, sizeof(grid));  // Restore the grid
	}
	
	memcpy (grid, gridbak, sizeof(grid));  // Ensure that the original grid was restored
	return over;
}
