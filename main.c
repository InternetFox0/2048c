
/* 
 * 2048c: Simple implementation of the 2048 game in C
 * By InternetFox0
 * 
 * File main.c: Simple driver program for the 2048 game logic
 * 
 * This code is made available under the terms of the MIT License.
 * See here for more info: https://gitlab.com/InternetFox0/2048c/blob/master/LICENSE
 */

#include "2048.h"

#include <stdio.h>

int main (void) {
	int result = 0;      // Temporary storage for random/push return status
	unsigned score = 0;  // The player's total score
	int select;          // Direction chosen
	
	// Create the game grid and place two tiles randomly:
	grid_init ();
	grid_rand ();
	grid_rand ();
	
	// Notify the user of the controls:
	printf ("0: Up\n1: Down\n2: Left\n3: Right\n");

	// Main loop:
	while (1) {
		grid_print ();                        // Print the current state of the grid
		printf ("%u> ", score);               // Print the score and prompt for a move
		scanf ("%d", &select);                // Read the move from the keyboard
		result = grid_push ((Dir_t) select);  // Try to push the grid in the selected direction
		if (result >= 0)                      // If that succeeded...
			score += result;                  // ...add the result to the score
		grid_rand ();                         // Add another random tile
		if (grid_ckover ()) break;            // Exit if no more moves can be made
	}

	printf ("Game over!\n");

	return 0;
}

