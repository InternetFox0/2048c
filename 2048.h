
/* 
 * 2048c: Simple implementation of the 2048 game in C
 * By InternetFox0
 * 
 * File 2048.h: Game function and data types declarations
 * 
 * This code is made available under the terms of the MIT License.
 * See here for more info: https://gitlab.com/InternetFox0/2048c/blob/master/LICENSE
 */

#ifndef __2048_H__
#define __2048_H__

// Game grid size:
#define SIDESZ 4                  // Number of tiles along one side of the grid
#define GRIDSZ (SIDESZ * SIDESZ)  // Total number of tiles - don't modify this manually!

// Directions:
// This creates a set of identifiers to represent each direction as an integer from 0 to 3
// The associated Dir_t data type can be used to restrict a variable to only allow a valid direction
typedef enum {UP = 0, DOWN, LEFT, RIGHT} Dir_t;

// Struct for creating a linked matrix of tiles:
typedef struct Tile_t_struct {
	unsigned              value;     // Number held by the tile
	struct Tile_t_struct  *next[4];  // Pointers to the tiles up, down, left and right from this one
} Tile_t;

// Function prototypes:
extern void grid_init   (void);       // Setup the tiles and their links
extern int  grid_ckfull (void);       // Check if the grid is full
extern int  grid_rand   (void);       // Randomly place either a 2 or 4 into the grid
extern void grid_print  (void);       // Print the current state of the grid
extern int  grid_push   (Dir_t dir);  // Push all tiles toward an edge in a selected direction
extern int  grid_ckover (void);       // Check if the game is over (no moves possible)

#endif // __2048_H__

