all: 2048c clean

2048c: main.o 2048.o
	${CC} $^ -o $@

clean:
	rm *.o
